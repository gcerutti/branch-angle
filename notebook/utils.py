import numpy as np

import vtk

from visu_core.vtk.polydata import vertex_scalar_property_polydata
from visu_core.matplotlib.colormap import to_rgb


class VtkLabelActorCells(vtk.vtkActor2D):
    def __init__(self, seg_img, color='k', font_size=40, cell_ids=None):
        super().__init__()

        self.seg_img = seg_img

        self.label_mapper = vtk.vtkLabeledDataMapper()
        self.SetMapper(self.label_mapper)

        self.update(color=color, font_size=font_size, cell_ids=cell_ids)

    def update(self, color='k', font_size=40, cell_ids=None):
        if cell_ids is None:
            cell_ids = self.seg_img.cell_ids()

        centers = self.seg_img.cells.barycenter()
        center_polydata = vertex_scalar_property_polydata({c: centers[c] for c in cell_ids},
                                                          np.array([c for c in cell_ids]))

        self.label_mapper.SetInputData(center_polydata)
        self.label_mapper.SetLabelModeToLabelScalars()
        self.label_mapper.SetLabelFormat("%g")
        self.label_mapper.GetLabelTextProperty().SetColor(*to_rgb(color))
        self.label_mapper.GetLabelTextProperty().SetFontSize(font_size)
        self.label_mapper.GetLabelTextProperty().ItalicOff()
        self.label_mapper.GetLabelTextProperty().SetJustificationToCentered()
        self.label_mapper.GetLabelTextProperty().SetVerticalJustificationToCentered()
        